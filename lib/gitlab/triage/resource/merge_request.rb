# frozen_string_literal: true

require_relative 'base'
require_relative 'shared/issuable'

module Gitlab
  module Triage
    module Resource
      class MergeRequest < Base
        include Shared::Issuable

        def reference
          '!'
        end
      end
    end
  end
end
