# frozen_string_literal: true

require 'spec_helper'

describe 'comment based on expanded labels' do
  include_context 'with integration context'

  let(:feature_issue) do
    issue.merge(labels: %w[feature], iid: issue[:iid] * 2)
  end

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, labels: 'bug' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end

    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, labels: 'feature' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [feature_issue]
    end
  end

  it 'comments on the issue' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              conditions:
                labels:
                  - |
                    {
                      bug,
                      feature
                    }
              actions:
                comment: |
                  Comment because {{labels}}
    YAML

    stub_post_bug = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
      body: { body: h('Comment because ~"bug"') },
      headers: { 'PRIVATE-TOKEN' => token })

    stub_post_feature = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{feature_issue[:iid]}/notes",
      body: { body: h('Comment because ~"feature"') },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post_bug)
    assert_requested(stub_post_feature)
  end
end
