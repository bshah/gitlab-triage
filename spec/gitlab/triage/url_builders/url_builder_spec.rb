require 'spec_helper'

require 'gitlab/triage/options'
require 'gitlab/triage/url_builders/url_builder'

describe Gitlab::Triage::UrlBuilders::UrlBuilder do
  let(:source_id) { 999 }
  let(:resource_type) { 'issues' }
  let(:resource_id) { 99 }
  let(:sub_resource_type) { 'notes' }
  let(:params) do
    {
      per_page: 50,
      milestones: "a,b"
    }
  end

  subject do
    described_class.new(
      network_options: Gitlab::Triage::Options.new,
      source: source,
      source_id: source_id,
      resource_type: resource_type,
      resource_id: resource_id,
      sub_resource_type: sub_resource_type,
      params: params
    ).build
  end

  context 'with project source' do
    let(:source) { 'projects' }

    describe '#build' do
      it 'produces the correct url' do
        expect(subject).to eq('https://gitlab.com/api/v4/projects/999/issues/99/notes?per_page=50&milestones=a,b')
      end

      context 'with a string ID' do
        let(:source_id) { 'gitlab-org/gitlab-ce' }

        it 'produces the correct url' do
          expect(subject).to eq('https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-ce/issues/99/notes?per_page=50&milestones=a,b')
        end
      end

      context 'with no sub resource' do
        let(:sub_resource_type) { nil }

        it 'produces the correct url' do
          expect(subject).to eq('https://gitlab.com/api/v4/projects/999/issues/99?per_page=50&milestones=a,b')
        end
      end

      context 'with no params' do
        let(:params) { nil }

        it 'produces the correct url' do
          expect(subject).to eq('https://gitlab.com/api/v4/projects/999/issues/99/notes')
        end
      end

      context 'with no resource_id' do
        let(:resource_id) { nil }

        it 'produces the correct url' do
          expect(subject).to eq('https://gitlab.com/api/v4/projects/999/issues/notes?per_page=50&milestones=a,b')
        end
      end
    end
  end

  context 'with group source' do
    let(:source) { 'groups' }
    let(:source_id) { 'gitlab-org/subgroup' }

    it 'produces the correct url' do
      expect(subject).to eq('https://gitlab.com/api/v4/groups/gitlab-org%2Fsubgroup/issues/99/notes?per_page=50&milestones=a,b')
    end
  end
end
